﻿using GOSTAPIs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace GOSTAPIs.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        public IHttpActionResult SignIn([FromBody]LoginModel loginModel)
        {
            MessageModel msg = new MessageModel();
            if (String.IsNullOrEmpty(loginModel.UserName) || String.IsNullOrEmpty(loginModel.Password))
            {
                msg.Message = "The username or password is empty1.";
            }
            else if (loginModel.UserName == "gost" && loginModel.Password == "gost")
            {
                msg.Message = "Logged in successfully.";
            }
            else
            {
                msg.Message = "The username or password is incorrect.";
            }
            return Ok<MessageModel>(msg);
        }
    }
}
