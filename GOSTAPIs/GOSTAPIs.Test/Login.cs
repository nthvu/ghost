﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GOSTAPIs.Controllers;
using GOSTAPIs.Models;
using System.Web.Http.Results;


namespace GOSTAPIs.Test
{
    [TestClass]
    public class Login
    {
        [TestMethod]
        public void Login_UserNameOrPasswordIncorrect()
        {
            var controller = new AccountController();

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "Phu";
            loginModel.Password = "Password";

            var result = controller.SignIn(loginModel) as OkNegotiatedContentResult<MessageModel>;
            Assert.AreEqual("The username or password is incorrect.", result.Content.Message);
        }

        [TestMethod]
        public void Login_UserNameOrPasswordEmpty()
        {
            var controller = new AccountController();

            LoginModel loginModel = new LoginModel();
           

            var result = controller.SignIn(loginModel) as OkNegotiatedContentResult<MessageModel>;
            Assert.AreEqual("The username or password is empty.", result.Content.Message);
        }

        [TestMethod]
        public void Login_UserNameOrPasswordCorrect()
        {
            var controller = new AccountController();

            LoginModel loginModel = new LoginModel();
            loginModel.UserName = "gost";
            loginModel.Password = "gost";


            var result = controller.SignIn(loginModel) as OkNegotiatedContentResult<MessageModel>;
            Assert.AreEqual("Logged in successfully.", result.Content.Message);
        }
    }
}
