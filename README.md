# Directory Layout #

```shell

├───GOSTAPIs                                                    #
│   ├───App_Start                                               #
│   ├───bin                                                     #
│   ├───Content                                                 #
│   │   └───themes                                              #
│   │       └───base                                            #
│   │           ├───images                                      #
│   │           └───minified                                    #
│   │               └───images                                  #
│   ├───Controllers                                             #
│   ├───Images                                                  #
│   ├───Models                                                  #
│   ├───obj                                                     #
│   │   └───Debug                                               #
│   ├───Properties                                              #
│   ├───Scripts                                                 #
│   └───Views                                                   #
│       ├───Home                                                #
│       └───Shared                                              #
├───GOSTAPIs.Test                                               #
└───packages                                                    #
    ├───EntityFramework.5.0.0                                   #
    ├───jQuery.1.7.1.1                                          #
    ├───jQuery.UI.Combined.1.8.20.1                             #
    ├───jQuery.Validation.1.9.0.1                               #
    ├───knockoutjs.2.1.0                                        #
    ├───Microsoft.AspNet.Mvc.4.0.20710.0                        #
    ├───Microsoft.AspNet.Providers.Core.1.1                     #
    ├───Microsoft.AspNet.Providers.LocalDB.1.1                  #
    ├───Microsoft.AspNet.Razor.2.0.20710.0                      #
    ├───Microsoft.AspNet.Web.Optimization.1.0.0                 #
    ├───Microsoft.AspNet.WebPages.2.0.20710.0                   #
    ├───Microsoft.jQuery.Unobtrusive.Ajax.2.0.20710.0           #
    ├───Microsoft.jQuery.Unobtrusive.Validation.2.0.20710.0     #
    ├───Microsoft.Web.Infrastructure.1.0.0.0                    #
    ├───Modernizr.2.5.3                                         #
    └───WebGrease.1.1.0                                         #

```
# Rule #
##A. Using Gitflow Workflow
The central repo holds two main branches with an infinite lifetime:

* master
* develop

[More detail](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow)
###I.	Branches
#####1.	When should you branch?
-	An epic: Have user story related to each other, do the same function
-	A user story: If an epic is so big including many other user stories, you can branch according to user story.
-	You have to  accept by branch’s manager
#####2.	Rule branches:
-       Always create branch from master or develop branch
-	Pull to get latest source
-	Branch Naming Conventions:

```shell
Feature branches
  feature/<id>-<description>
```
```shell
Release branches
  release/<version>-<description>
```
```shell
Bug fix branch
  bugfix/<id>-<description>
```
```shell
Hot fix branch
  hotfix/<id>-<description>
```
[More detail](https://confluence.atlassian.com/bitbucketserver/using-branches-in-bitbucket-server-776639968.html#UsingbranchesinBitbucketServer-settings)
###II. Git Commit
#####1.   When should you commit?
-  Unit tests pass.
-  Right code convention

#####2.  Git commit message.
* Separate subject from body with a blank line
* Limit the subject line to 50 characters
* Capitalize the subject line
* Do not end the subject line with a period
* Use the imperative mood in the subject line
* Wrap the body at 72 characters
* Use the body to explain what and why vs. how

     [More detail](https://chris.beams.io/posts/git-commit/ )

###III. Git Merge
*  Pass unit test
*  Right code convention
*  Rebase from develop branch to get latest source and resolve conflict (if have).
+  Create pull request
    * Make it small
    * Do only one thing
    * Watch your line width
    * Avoid re-formatting
    * Make sure the code builds          
    * Make sure all tests pass
    * Add tests
    * Document your reasoning

         [More detail ](http://blog.ploeh.dk/2015/01/15/10-tips-for-better-pull-requests/)

* Accepted by the responsible person

Practice http://learngitbranching.js.org/

##B. Unit Test
###1. [Unit Testing Guidelines](http://geosoft.no/development/unittesting.html)

###2. Naming standards
Three main parts:

__[MethodName_StateUnderTest_ExpectedBehavior]__

Example: 

Public void Sum_NegativeNumberAs1stParam_ExceptionThrown()

Public void Sum_simpleValues_Calculated ()

Public void Parse_SingleToken_ReturnsEqualToeknValue ()

>When a unit test fails, I want to understand what I just broke — without reading the test code.

[More detail](http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html)


# Prerequisites #

*
*
*


# Getting Started #

**Step 1**. Git Pull the latest source code
```shell

$ git clone https://youraccount@bitbucket.org/tamphthanh/gost.git

```

**Step 2**. do actions for develop, debug...
1. get
1. set

# How to Deploy #
**Step 1**. Prepare
1. get
1. set

# Contributors
A special thanks to:

* [![Anh Tran](https://bitbucket.org/account/anhductran/avatar/32/)](https://bitbucket.org/anhductran/) *Anh Tran* is a big guys for CI/CD
* [![Vu Nguyen](https://bitbucket.org/account/nthvu/avatar/32/)](https://bitbucket.org/nthvu/) *Vu Nguyen* is enthusiast in applying Git methodology
* [![Truong Pham](https://bitbucket.org/account/truph/avatar/32/)](https://bitbucket.org/truph/) *Truong Pham* burns his engergy on product management perspective
* [![Phu Doan](https://bitbucket.org/account/doanbaphu/avatar/32/)](https://bitbucket.org/doanbaphu/) *Phu Doan* coordinate between the team 

# Additional resources

# FAQ

---
Made with ♥ by [![Tam Pham](https://bitbucket.org/account/tamphthanh/avatar/256/?ts=1492586875)](https://bitbucket.org/tamphthanh/)